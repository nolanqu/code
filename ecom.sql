/*
ecom-acquisition

Online acquisition model
Database: big-query

Selection of customers and targets as part of previous online acquisition campaigns (pickup / delivery models)

Author: J Huvanandana, Nolan Qu
Estimated run time: 5 minutes
Last updated: 25 Apr 2020
*/

--------------------------------------------------------------------------------------------------------
-- 1. Splits from campaign codes
--------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS yxq.onl_acq_campaigns;
CREATE TABLE yxq.onl_acq_campaigns AS
	SELECT campaign_code
		  ,campaign_name
		  ,MIN(week_promo_start) as week_promo_start
		  ,MAX(week_promo_end) as week_promo_end
	  FROM loyalty_bi_analytics.dim_campaign
	 WHERE campaign_code IN (
	 	'LCP-0120'
	   ,'CVM-0174'
	   ,'CVM-0127'
	   ,'CVM-1815'
	   ,'CVM-1891'
	   ,'CVM-2089'
	   ,'CVM-2292'
	   ,'CVM-2358'
	   ,'CVM-2533'
	   ,'CVM-2682'
	   ,'CVM-2783'
	   ,'CVM-2841'
	   ,'CVM-2969'
	 )
	 GROUP BY 1,2
;

DROP TABLE IF EXISTS yxq.onl_acq_segments;
CREATE TABLE yxq.onl_acq_segments AS
    WITH segments AS (
        SELECT c.campaign_code
              ,c.campaign_name
              ,c.week_promo_start
              ,c.week_promo_end
              ,ca.crn
              ,ca.segment_code
              ,ca.target_control
              ,UPPER(ca.channel_type) AS channel
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(0)] AS segment_1
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(1)] AS segment_2
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(2)] AS segment_3
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(3)] AS segment_4
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(4)] AS segment_5
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(5)] AS segment_6
              ,SPLIT(segment_code, '|')[SAFE_OFFSET(6)] AS segment_7
              ,MIN(ca.campaign_or_control_start_date) AS campaign_start_date
              ,MAX(ca.campaign_or_control_end_date) AS campaign_end_date

          FROM yxq.onl_acq_campaigns AS c

        -- get campaign code
        LEFT JOIN loyalty.ca_campaigns_audience_list as ca
            ON c.campaign_code = ca.campaign_code

        WHERE UPPER(ca.channel_type) = 'EDM'

        GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
    )
    SELECT s.campaign_code
          ,s.campaign_name
          ,s.week_promo_start
          ,s.week_promo_end
          ,s.crn
          ,s.segment_code
          ,CASE
            WHEN s.campaign_code = 'CVM-1815' AND s.segment_1 = 'EM' and s.segment_2 = 'acquistion' and s.segment_3 = 'pick-up:non-random' THEN 'pickup'
            WHEN s.campaign_code = 'CVM-1891' and s.segment_2 = ' Model: Pick-up' THEN 'pickup:non-random'
            WHEN s.campaign_code = 'CVM-2089' THEN 'pickup:non-random'
            WHEN s.campaign_code = 'CVM-2292' AND s.segment_1 = 'EM' and s.segment_3 = 'Model-Pick-Up' THEN 'pickup:non-random'
            WHEN s.campaign_code = 'CVM-1815' AND s.segment_1 = 'EM' and s.segment_2 = 'acquistion' AND s.segment_3 = 'cfc' THEN 'delivery:non-random'
            WHEN s.campaign_code = 'CVM-1891' AND s.segment_2 = ' Model: Delivery' THEN 'delivery:non-random'
            WHEN s.campaign_code = 'CVM-2292' AND s.segment_1 = 'EM' AND s.segment_3 = 'Model-Delivery' THEN 'delivery:non-random'
            WHEN s.campaign_code = 'CVM-1815' AND s.segment_3 = 'pick-up' THEN 'pickup:random'
            WHEN s.campaign_code = 'CVM-1891' AND s.segment_1 = 'Segment-4' THEN 'pickup:random'
            WHEN s.campaign_code = 'CVM-2089' AND s.segment_2 = 'Random' THEN 'pickup:random'
            WHEN s.campaign_code = 'CVM-1815' AND s.segment_3 = 'cfc' THEN 'delivery:random'
            WHEN s.campaign_code = 'CVM-1891' AND s.segment_1 = 'Segment-2' THEN 'delivery:random'
            ELSE 'ERR'
            END camp_seg
            
      FROM segments AS s
;

--------------------------------------------------------------------------------------------------------
-- 1. Code from Hans notebook
--------------------------------------------------------------------------------------------------------
#('CVM-2292', 'CVM-2089', 'CVM-1815', 'CVM-1891', 'CVM-2358')
#('CVM-2783', 'CVM-2841', 'CVM-2682', 'CVM-2533')
select 
    dc.campaign_code,
    dc.campaign_name,
    min(dc.week_promo_start) as week_promo_start,
    max(dc.week_promo_end) as week_promo_end
from 
    loyalty_bi_analytics.dim_campaign dc
where 
    campaign_code = 'CVM-2358'
group by
    1, 2
order by 
    week_promo_start
;

DROP TABLE IF EXISTS yxq.goo_old;
create table yxq.goo_old as
select 
    crn, campaign_code, campaign_audience_type, 
    min(offer_start_date) as offer_start_date, max(offer_end_date) as offer_end_date,
    min(tot_amt_excld_gst_l4w) as tot_amt_excld_gst_l4w,
    min(tot_amt_excld_gst_post1 + tot_amt_excld_gst_post2 + tot_amt_excld_gst_post3 + tot_amt_excld_gst_post4) as tot_amt_excld_gst_p4w,
    LEAST(SUM(CASE WHEN send_flag is not null THEN 1 ELSE 0 END), 1) as send_flag,
    LEAST(SUM(CASE WHEN open_flag is not null THEN 1 ELSE 0 END), 1) as open_flag,
    LEAST(SUM(CASE WHEN activate_flag is not null THEN 1 ELSE 0 END), 1) as activate_flag,
    LEAST(SUM(CASE WHEN redeem_flag is not null THEN 1 ELSE 0 END), 1) as redeem_flag,
    LEAST(SUM(CASE WHEN shop_online_flag is not null THEN 1 ELSE 0 END), 1) as shop_online_flag
from 
    loyalty_bi_analytics.fact_campaign_sales
where 
    campaign_code in ('CVM-2292', 'CVM-2089', 'CVM-1815', 'CVM-1891', 'CVM-2358')
group by 
    1, 2, 3;

DROP TABLE IF EXISTS yxq.goo1_old;
create table yxq.goo1_old as
select 
    crn, campaign_code, segment_code,
    SPLIT(segment_code, '|')[SAFE_OFFSET(0)] as segment_code_1,
    SPLIT(segment_code, '|')[SAFE_OFFSET(1)] as segment_code_2,
    SPLIT(segment_code, '|')[SAFE_OFFSET(2)] as segment_code_3,
    SPLIT(segment_code, '|')[SAFE_OFFSET(3)] as segment_code_4
from
    loyalty.ca_campaigns_audience_list
where 
    campaign_code in ('CVM-2292', 'CVM-2089', 'CVM-1815', 'CVM-1891', 'CVM-2358')
    and channel_type = 'eDM'
;

--no pick up table ie goo2 in Hans code
drop table if exists yxq.nonrandom_old;
create table yxq.nonrandom_old as
select 
    a.*,
    date_add(a.offer_start_date,INTERVAL -8 DAY) as ref_dt,
    segment_code,
    b.segment_code_1,
    b.segment_code_2,
    b.segment_code_3,
    b.segment_code_4
from 
    yxq.goo_old as a
left join 
    yxq.goo1_old as b
    on a.crn = b.crn and a.campaign_code = b.campaign_code
where
    a.send_flag = 1
    and (
        (b.campaign_code = 'CVM-1815' and b.segment_code_1 = 'EM' and b.segment_code_2 = 'acquistion' 
        and b.segment_code_4 in ('Model: Acquisition - Delivery', 'Model: Acquisition - Pick Up')) or
    (b.campaign_code = 'CVM-1891' and b.segment_code_2 in (' Model: Delivery', ' Model: Pick-up')
        and b.segment_code_1 in ('Segment-2', 'Segment-4')) or
    (b.campaign_code = 'CVM-2089' and b.segment_code in ('Acquisition |Model', 'Pick-Up Trial')) or
    (b.campaign_code = 'CVM-2292' and b.segment_code_1 = 'EM' and 
        b.segment_code_3 in ('Model-Delivery', 'Model-Pick-Up')) or
    (b.campaign_code = 'CVM-2358' and b.segment_code in ('1', '2', '3'))
    )
;



drop table if exists yxq.random_old;
create table yxq.random_old as
select 
    a.*,
    date_add(a.offer_start_date,INTERVAL -8 DAY) as ref_dt,
    segment_code,
    b.segment_code_1,
    b.segment_code_2,
    b.segment_code_3,
    b.segment_code_4
from 
    yxq.goo_old as a
left join 
    yxq.goo1_old as b
    on a.crn = b.crn and a.campaign_code = b.campaign_code
where
    a.send_flag = 1
    and (
    (b.campaign_code = 'CVM-1815' and b.segment_code_1 = 'EM' and b.segment_code_2 = 'acquistion' 
        and b.segment_code_3 in ('cfc', 'pick-up') and b.segment_code_4 = 'Random') or
    (b.campaign_code = 'CVM-1891' and b.segment_code_2 = ' Model: Random' 
        and b.segment_code_1 in ('Segment-2', 'Segment-4')) or
    (b.campaign_code = 'CVM-2089' and b.segment_code = 'Acquisition |Random') or
    (b.campaign_code = 'CVM-2292' and b.segment_code_1 = 'EM' and b.segment_code_3 = 'Model-Random') or
    (b.campaign_code = 'CVM-2358' and b.segment_code in ('1T', '2T', '3T'))
    )
;

select 
    'pick' as model, 'non-random' as group,
    campaign_code, crn, ref_dt, 
    case when activate_flag = 1 and redeem_flag = 1 then 1 else 0 end as target
from 
    yxq.nonrandom_old
where 
    campaign_audience_type = 'T' and (
    (campaign_code = 'CVM-1815' and segment_code_1 = 'EM' and segment_code_2 = 'acquistion' 
        and segment_code_3 = 'pick-up') or 
    (campaign_code = 'CVM-1891' and segment_code_2 = ' Model: Pick-up') or
    (campaign_code = 'CVM-2089') or
    (campaign_code = 'CVM-2292' and segment_code_1 = 'EM' and segment_code_3 = 'Model-Pick-Up')
    )